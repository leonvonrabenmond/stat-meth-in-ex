{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Error propagation\n",
    "\n",
    "a) Propagate uncertainties for the following expressions using [SymPy](https://www.sympy.org) following the examples for [uncorrelated variables](https://nbviewer.jupyter.org/urls/www.physi.uni-heidelberg.de/Einrichtungen/FP/Datenanalyse/FP_Gaussian_error_propagation.ipynb?flush_cache=false) and [correlated variables](https://nbviewer.jupyter.org/urls/www.physi.uni-heidelberg.de/Einrichtungen/FP/Datenanalyse/FP_Gaussian_error_propagation_corr.ipynb?flush_cache=false) from the FP web page.\n",
    "\n",
    "i) Find expressions for the absolute uncertainty $\\sigma_z$ for $z = x + y$ and $z = x - y$ \n",
    "\n",
    "ii) Find expressions for the relative uncertainty $\\sigma_z / z$ for $z = x \\cdot y, \\; z = x / y$ and $z = x^n y^n$\n",
    "\n",
    "iii) The acceleration of gravity with a simple pendulum is given by the following formula:\n",
    "$$g = 4  \\pi^2 \\frac{L}{T^2}$$\n",
    "The relevant variables are the length $L$ of the pendulum and the period $T$ with the corresponding errors $\\sigma_L$ and $\\sigma_T$.\n",
    "\n",
    "b) The radius $r$ and the height $h$ of a cylinder have been measured to $r = 2$ cm and $h = 3$ cm. The uncertainty for both measurements is $\\sigma = 0.05$ cm. Determine the volume of the cylinder and its uncertainty assuming (i) that both measurements are uncorrelated and (ii) that both measurements are fully correlated.\n",
    "\n",
    "c) The scattering angle and the radial distance of a certain particle can be determined from a position measurement $(x,y)$ \n",
    "$$r = \\sqrt{x^2 + y^2}, \\quad \\theta = \\mathrm{atan2}(y, x)$$\n",
    "You find more on the [atan2](https://en.wikipedia.org/wiki/Atan2) function on wikipedia. The position ($x$,$y$) is measured with the corresponding uncertainties $\\sigma_x$ and $\\sigma_y$. Write a python function that returns the covariance matrix $U$ of $r$ and $\\theta$ for a given covariance matrix $V$ of $x$ and $y$. Determine $U$ under the assumption that $x$ and $y$ are uncorrelated. Hint: The formulas you need can be found in the script.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sympy import *\n",
    "from IPython.display import display, Latex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At first we will write functions that calculate the uncertainties for any given expression, depending on whether or not the variables are correlated. For this we are assuming gaussian errors, and the formula we are using is\n",
    "$$ \\sigma f(x_1, ..., x_n) = \\sqrt{\\sum_{i=1}^n \\left(\\frac{\\partial f}{\\partial x_i}\\cdot\\sigma_i \\right) ^2} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def error_prop(f, vars):\n",
    "    \"\"\"\n",
    "    Symbolic Gaussian error propagation.\n",
    "    \n",
    "    Arguments:\n",
    "    f: formula (sympy expression)\n",
    "    vars: list of independent variables and corresponding uncertainties [(x1, sigma_x1), (x2, sigma_x2), ...]\n",
    "    \n",
    "    return value: sympy expression for the uncertainty of the calculated expression f\n",
    "    \n",
    "    \"\"\"\n",
    "    sum = S(0) # empty SymPy expression\n",
    "    for (x, sigma) in vars:\n",
    "        sum += diff(f, x)**2 * sigma**2 \n",
    "    return sqrt(simplify(sum))\n",
    "\n",
    "\n",
    "\n",
    "def error_prop_corr(f, x, V):\n",
    "    \"\"\"\n",
    "    f: function f = f(x[0], x[1], ...)\n",
    "    x: list of variables\n",
    "    V: covariance matrix (python 2d list)\n",
    "    \"\"\"\n",
    "    sum = S(0) # empty sympy expression\n",
    "    for i in range(len(x)):\n",
    "        for j in range(len(x)):\n",
    "            sum += diff(f, x[i]) * diff(f, x[j]) * V[i][j] \n",
    "    return sqrt(simplify(sum))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "a.) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(i) For $z=x+y$, with the variables $x,y$ correlated through $\\rho$ the absolute uncertainty of $z$ is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\sqrt{2 \\rho \\sigma_{x} \\sigma_{y} + \\sigma_{x}^{2} + \\sigma_{y}^{2}}$"
      ],
      "text/plain": [
       "sqrt(2*rho*sigma_x*sigma_y + sigma_x**2 + sigma_y**2)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x, y = symbols('x, y', real=True)\n",
    "sigma_x, sigma_y = symbols('sigma_x, sigma_y', positive=True)\n",
    "rho = Symbol(\"rho\", real=True)\n",
    "\n",
    "# covariance matrix\n",
    "C = [[sigma_x**2, rho * sigma_x * sigma_y], [rho * sigma_x * sigma_y, sigma_y**2]]\n",
    "\n",
    "z = x + y\n",
    "vars = [x, y]\n",
    "sigma_z = error_prop_corr(z, vars, C)\n",
    "sigma_z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For $z=x-y$, with the variables $x,y$ correlated through $\\rho$ the absolute uncertainty of $z$ is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\sqrt{- 2 \\rho \\sigma_{x} \\sigma_{y} + \\sigma_{x}^{2} + \\sigma_{y}^{2}}$"
      ],
      "text/plain": [
       "sqrt(-2*rho*sigma_x*sigma_y + sigma_x**2 + sigma_y**2)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "z = x - y\n",
    "sigma_z = error_prop_corr(z, vars, C)\n",
    "sigma_z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(ii) For $z=x\\cdot y$ the relative uncertainty is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\frac{\\sqrt{2 \\rho \\sigma_{x} \\sigma_{y} x y + \\sigma_{x}^{2} y^{2} + \\sigma_{y}^{2} x^{2}}}{x y}$"
      ],
      "text/plain": [
       "sqrt(2*rho*sigma_x*sigma_y*x*y + sigma_x**2*y**2 + sigma_y**2*x**2)/(x*y)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x, y = symbols('x, y', real=True)\n",
    "sigma_x, sigma_y = symbols('sigma_x, sigma_y', positive=True)\n",
    "rho = Symbol(\"rho\", real=True)\n",
    "\n",
    "# covariance matrix\n",
    "C = [[sigma_x**2, rho * sigma_x * sigma_y], [rho * sigma_x * sigma_y, sigma_y**2]]\n",
    "\n",
    "z = x * y\n",
    "vars = [x, y]\n",
    "sigma_z = error_prop_corr(z, vars, C)\n",
    "simplify(sigma_z/z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is equal to \n",
    "$$ \\sqrt{\\frac{2\\rho \\sigma_x \\sigma_y}{ xy} + \\frac{\\sigma_y^2}{y^2} + \\frac{\\sigma_x^2}{x^2}} $$\n",
    "\n",
    "For $z=x/y$ the uncertainty is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\frac{y \\sqrt{\\frac{- 2 \\rho \\sigma_{x} \\sigma_{y} x y + \\sigma_{x}^{2} y^{2} + \\sigma_{y}^{2} x^{2}}{y^{4}}}}{x}$"
      ],
      "text/plain": [
       "y*sqrt((-2*rho*sigma_x*sigma_y*x*y + sigma_x**2*y**2 + sigma_y**2*x**2)/y**4)/x"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x, y = symbols('x, y', real=True)\n",
    "sigma_x, sigma_y = symbols('sigma_x, sigma_y', positive=True)\n",
    "rho = Symbol(\"rho\", real=True)\n",
    "\n",
    "# covariance matrix\n",
    "C = [[sigma_x**2, rho * sigma_x * sigma_y], [rho * sigma_x * sigma_y, sigma_y**2]]\n",
    "\n",
    "z = x / y\n",
    "vars = [x, y]\n",
    "sigma_z = error_prop_corr(z, vars, C)\n",
    "simplify(sigma_z/z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is equal to \n",
    "$$ \\frac{\\sqrt{-2\\rho \\sigma_x \\sigma_y xy + \\sigma_y^2x^2 + \\sigma_x^2y^2}}{xy} = \\sqrt{-\\frac{2\\rho \\sigma_x \\sigma_y}{ xy} + \\frac{\\sigma_y^2}{y^2} + \\frac{\\sigma_x^2}{x^2}} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For $z=x^n y^n$ the relative uncertainty is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle x^{- n} y^{- n} \\sqrt{x^{2 n - 2} y^{2 n - 2} \\cdot \\left(2 \\rho \\sigma_{x} \\sigma_{y} x y + \\sigma_{x}^{2} y^{2} + \\sigma_{y}^{2} x^{2}\\right)} \\left|{n}\\right|$"
      ],
      "text/plain": [
       "sqrt(x**(2*n - 2)*y**(2*n - 2)*(2*rho*sigma_x*sigma_y*x*y + sigma_x**2*y**2 + sigma_y**2*x**2))*Abs(n)/(x**n*y**n)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x, y, n = symbols('x, y, n', real=True)\n",
    "sigma_x, sigma_y = symbols('sigma_x, sigma_y', positive=True)\n",
    "rho = Symbol(\"rho\", real=True)\n",
    "\n",
    "# covariance matrix\n",
    "C = [[sigma_x**2, rho * sigma_x * sigma_y], [rho * sigma_x * sigma_y, sigma_y**2]]\n",
    "\n",
    "z = x**n * y**n\n",
    "vars = [x, y]\n",
    "sigma_z = error_prop_corr(z, vars, C)\n",
    "simplify(sigma_z/z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(iii) The uncertainty of the gravitatinal acceleration on a pendulum\n",
    "$$ g = 4\\pi^2 \\frac{L}{T^2} $$\n",
    "is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\frac{4 \\pi^{2} \\sqrt{4 L^{2} \\sigma_{T}^{2} - 4 L T \\rho \\sigma_{L} \\sigma_{T} + T^{2} \\sigma_{L}^{2}}}{T^{3}}$"
      ],
      "text/plain": [
       "4*pi**2*sqrt(4*L**2*sigma_T**2 - 4*L*T*rho*sigma_L*sigma_T + T**2*sigma_L**2)/T**3"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "L, T = symbols('L, T', positive=True)\n",
    "sigma_L, sigma_T = symbols('sigma_L, sigma_T', positive=True)\n",
    "rho = Symbol('rho', real=True)\n",
    "\n",
    "# covariance matrix\n",
    "C = [[sigma_L**2, rho * sigma_L * sigma_T], [rho * sigma_L * sigma_T, sigma_T**2]]\n",
    "\n",
    "g = 4*pi**2 *L/T**2\n",
    "vars = [L, T]\n",
    "sigma_g = error_prop_corr(g, vars, C)\n",
    "sigma_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "b.) From given measurements of a cylinders height $h=3cm$ and radius $r=2$cm with uncertainties $\\sigma_r = \\sigma_h = \\sigma$, the capacity (German: 'Volumen') of the cylinder can be calculated using the equation\n",
    "$$ V = \\pi r^2 h $$\n",
    "The uncertainty of the capacity calculatet in this way is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\pi \\sigma_{rh} r \\sqrt{4 h^{2} + 4 h r \\rho + r^{2}}$"
      ],
      "text/plain": [
       "pi*\\sigma_{rh}*r*sqrt(4*h**2 + 4*h*r*rho + r**2)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "r,h = symbols('r, h', positive=True)\n",
    "sigma_rh = Symbol('\\sigma_{rh}', positive=True)\n",
    "rho = Symbol('rho', real=True)\n",
    "\n",
    "# covariance matrix\n",
    "C = [[sigma_rh**2, rho * sigma_rh**2], [rho * sigma_rh**2, sigma_rh**2]]\n",
    "\n",
    "V = pi* r**2 *h\n",
    "vars = [r, h]\n",
    "sigma_V = error_prop_corr(V, vars, C)\n",
    "sigma_V"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "V = 37.6991118430775 cm^3\n"
     ]
    }
   ],
   "source": [
    "# plugging in the given measurements\n",
    "\n",
    "r_meas = 2 # cm\n",
    "h_meas = 3 # cm\n",
    "sigma_meas = 0.05 # cm\n",
    "\n",
    "central_value = V.subs([(r,r_meas), (h, h_meas)]).evalf()\n",
    "print('V = ' + str(central_value) + ' cm^3')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sigma_uncorr = 1.98691765315922 cm^3\n"
     ]
    }
   ],
   "source": [
    "# calculating sigma_V for uncorrelated measurements (rho=0)\n",
    "\n",
    "sigma_uncorr = sigma_V.subs([(r, r_meas), (h, h_meas), (sigma_rh, sigma_meas), (rho, 0)]).evalf()\n",
    "print('sigma_uncorr = ' + str(sigma_uncorr) + ' cm^3')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sigma_corr = 2.51327412287183 cm^3\n"
     ]
    }
   ],
   "source": [
    "# calculating sigma_V for compltetely correlated measurements (rho=1)\n",
    "\n",
    "sigma_corr = sigma_V.subs([(r, r_meas), (h, h_meas), (sigma_rh, sigma_meas), (rho, 1)]).evalf()\n",
    "print('sigma_corr = ' + str(sigma_corr) + ' cm^3')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "c.) Now we will write a function which, given the covariance matrix of two measured quantities, returns the covariance matrix of two variables depending on the quantities. In this example we have\n",
    "$$ r = \\sqrt{x^2 + y^2} \\quad\\text{ and }\\quad \\theta = \\text{atan2}(x,y) = \\text{arctan}\\left(\\frac{y}{x} \\right)$$\n",
    "where \n",
    "$$ V = \\begin{pmatrix} \\sigma_x^2 & \\rho \\sigma_x \\sigma_y \\\\ \\rho \\sigma_x \\sigma_y & \\sigma_y^2 \\end{pmatrix} $$\n",
    "is the covariance matrix of $x$ and $y$.\n",
    "In the script (page 21), the transformation of the covariance matrix $C$ to the covariance matrix $U$ of $r$ and $\\theta$ is explained. This is done ba determining the Transformation matrix $G$\n",
    "$$ G_{ki} = \\frac{\\partial f_k}{\\partial x_i} $$\n",
    "where in our case $f_1=r$, $f_2=\\theta$, $x_1=x$ and $x_2 = y$.\n",
    "Through multipliing with the transformation matrix, one can calculate the new covariance matrix\n",
    "$$ U = G \\cdot C \\cdot G^T $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\left[\\begin{array}{cc}\\left(- \\frac{C_{0, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{0, 0} x_{0, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{0, 0} + \\left(- \\frac{C_{0, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{0, 0} x_{1, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{0, 1} & \\left(- \\frac{C_{0, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{0, 0} x_{0, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{1, 0} + \\left(- \\frac{C_{0, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{0, 0} x_{1, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{1, 1}\\\\\\left(- \\frac{C_{1, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{1, 0} x_{0, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{0, 0} + \\left(- \\frac{C_{1, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{1, 0} x_{1, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{0, 1} & \\left(- \\frac{C_{1, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{1, 0} x_{0, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{1, 0} + \\left(- \\frac{C_{1, 1} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} + \\frac{C_{1, 0} x_{1, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\right) C_{1, 1}\\end{array}\\right]$"
      ],
      "text/plain": [
       "Matrix([\n",
       "[(-C[0, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[0, 0]*x[0, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[0, 0] + (-C[0, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[0, 0]*x[1, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[0, 1], (-C[0, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[0, 0]*x[0, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[1, 0] + (-C[0, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[0, 0]*x[1, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[1, 1]],\n",
       "[(-C[1, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[1, 0]*x[0, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[0, 0] + (-C[1, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[1, 0]*x[1, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[0, 1], (-C[1, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[1, 0]*x[0, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[1, 0] + (-C[1, 1]*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2) + C[1, 0]*x[1, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2))*C[1, 1]]])"
      ]
     },
     "execution_count": 65,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "x,y = symbols('x,y', real=True)\n",
    "C = MatrixSymbol('C', 2, 2)\n",
    "\n",
    "r = sqrt(x**2 + y**2)\n",
    "theta = atan2(x, y)\n",
    "\n",
    "def cov_trafo(x, y, C):\n",
    "    ''' Given the Covariance Matrix C of x and y, cov_trafo \n",
    "    calculates the covariance matrix of the dependant values\n",
    "    r and theta.'''\n",
    "    G = MatrixSymbol('G', 2, 2)\n",
    "    U = MatrixSymbol('U', 2, 2)\n",
    "    G_11 = diff(r,x[0])\n",
    "    G_12 = diff(r,x[1])\n",
    "    G_21 = diff(theta, x[0])\n",
    "    G_21 = diff(theta, x[1])\n",
    "    G = Matrix([[G_11, G_12], [G_21, G_22]])\n",
    "    E = Matrix(C*G*C.T)\n",
    "    return E\n",
    "\n",
    "cov_trafo(x,C)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 67,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\left[\\begin{matrix}\\frac{\\sigma_{x}^{4} x_{0, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}} & \\frac{\\sigma_{x}^{2} \\sigma_{y}^{2} x_{1, 0}}{\\sqrt{x_{0, 0}^{2} + x_{1, 0}^{2}}}\\\\- \\frac{\\sigma_{x}^{2} \\sigma_{y}^{2} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}} & - \\frac{\\sigma_{y}^{4} x_{0, 0}}{x_{0, 0}^{2} + x_{1, 0}^{2}}\\end{matrix}\\right]$"
      ],
      "text/plain": [
       "Matrix([\n",
       "[        sigma_x**4*x[0, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2), sigma_x**2*sigma_y**2*x[1, 0]/sqrt(x[0, 0]**2 + x[1, 0]**2)],\n",
       "[-sigma_x**2*sigma_y**2*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2),               -sigma_y**4*x[0, 0]/(x[0, 0]**2 + x[1, 0]**2)]])"
      ]
     },
     "execution_count": 67,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# now, instead of a general covariance matrix C, we will \n",
    "# use that of completely uncorrelated variables x and y -> rho=0\n",
    "\n",
    "sigma_x, sigma_y = symbols('sigma_x, sigma_y', positive =True)\n",
    "C_uncorr = Matrix([[sigma_x**2, 0], [0, sigma_y**2]])\n",
    "cov_trafo(x, C_uncorr)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
